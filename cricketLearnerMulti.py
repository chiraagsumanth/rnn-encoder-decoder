from enc_dec_gru_multitask_learning import EncoderDecoderRnn
from nltk import word_tokenize, sent_tokenize
import numpy
import pickle
#from gensim.models import Word2Vec
import os
import re
from itertools import permutations
from random import shuffle
import copy
import time

pickle_file = "pickles/cricket_model.pickle"

'''def getPermutations(inp):
    l = len(inp)
    indices = [i for i in rangle(l)]
    perm = []
    for i in permutations(indices, l):
        perm.append(i) # perm gives all possible indices for permuting'''

def getPermutations(inp, out):
    split1 = [[i[0],i] for i in inp if i[7]==0]
    split2 = [[i[7],i] for i in inp if not i[7]==0]
    
    perm1 = [i for i in permutations(split1, len(split1))] # perm gives all possible indices for permuting 
    perm2 = [i for i in permutations(split2, len(split2))]
    shuffle(perm1)
    shuffle(perm2)
    perm1 = perm1[:500]
    perm2 = perm2[:500]
    
    permuted1 = []
    permuted2 = []
    
    for i in range(len(perm1)):
        tem = []
        for j in range(len(perm1[i])):
            t = copy.deepcopy(perm1[i][j][1])
            
            t[0] = j
            tem.append(t)
        permuted1.append(tem)
        
    for i in range(len(perm2)):
        tem = []
        for j in range(len(perm2[i])):
            t = copy.deepcopy(perm2[i][j][1])
            t[7] = j+11 # for bowlers
            tem.append(t)
        permuted2.append(tem)
    
    res = [[i+permuted2[0], out] for i in permuted1]+[[permuted1[0]+i, out] for i in permuted2]
    return res

def getWord2Vec(word, wordvec_model, wordvec_dim = 30):
    try:
        return wordvec_model[word]
    except:
        return [0] * wordvec_dim

def trainWord2Vec(wordvec_pickle = "./pickles/wordvec.pickle", input_file = "./corpora/inp.txt", wordvec_dim = 30):
    input_text = open(input_file).read()
    print "  File read  ".center(50, '*')
    if os.path.exists(wordvec_pickle):
        print "  Word2Vec already trained  ".center(50, '*')
        model = pickle.load(open(wordvec_pickle))
        print "  Word2Vec model loaded  ".center(50, '*')
    else:
        sentences = [re.split(r"\W+", sent) for sent in sent_tokenize(input_text)]
        model = Word2Vec(sentences, min_count=1, size=wordvec_dim, window=4)
        print "  Word2Vec trained  ".center(50, '*')
        pickle.dump(model, open(wordvec_pickle, "w"))
        print "  Word2Vec model pickled  ".center(50, '*')
        

def train(nx1, nh1, ny1, ny21, nx2, nh2, ny2, ny22, enc_inputs, enc_targets, dec_inputs, dec_targets, rnn_pickle = "./pickles/rnn_cricket.pickle"):
    rnn = EncoderDecoderRnn([nx1, nh1, ny1, ny21], [nx2, nh2, ny2, ny22], output_layer=None)
    
    if os.path.exists(rnn_pickle):
        print "  RNN already trained and pickled  ".center(50, '*')
        return pickle.load(open(rnn_pickle))
    else:
        print "  RNN training...  ".center(50, '*')
        rnn.train(enc_inputs, enc_targets, dec_inputs, dec_targets, max_iter = 2)
        print "  RNN trained  ".center(50, '*')
        pickle.dump(rnn, open(rnn_pickle, "wb"))
        print "  RNN pickled  ".center(50, '*')
        return rnn
        
def test(inputs1, targets=None, wordvec_dim = 30, wordvec_pickle = "./pickles/wordvec.pickle", rnn_pickle = "./pickles/rnn_cricket_v2.pickle"):
    count = 0
    total = 0
    count1 = 0
    total1 = 0
    rnn = pickle.load(open(rnn_pickle))
    inputs = []
    for inp1 in inputs1:
    	inputs.append([list(map(lambda x: float(x) if(type(x)==unicode and ((not (x=='-')) and (not(x[0]==" ")))) else (0 if type(x) == unicode else x), inp)) for inp in inp1 if len(inp)==13])
    print "inputs =", inputs
    
    t, _, results = rnn.test_predict(inputs)
    if targets is not None:
        for a, b in zip(results,targets):
            # if a == b:
                # count+=1
            total+= len(set(zip(*a)[0])|set(zip(*b)[0]))
            count+= len(set(zip(*a)[0])&set(zip(*b)[0]))
            
            total1+= len(set(zip(*a)[1])|set(zip(*b)[1]))
            count1+= len(set(zip(*a)[1])&set(zip(*b)[1]))
                
        print "Accuracy message type =", count/float(total) *100.0
        print "Accuracy index type =", count1/float(total1) *100.0
    return results
    
def create_dataset(wordvec_model, wordvec_dim = 30, num_messages = 25, num_players = 22):
    inputs = [["This", "is", "a", "sample", "sentence"]] # list of all match reports [[report1 words],[report2 words]]
    outputs = [[1,2,3,4,5]] # list of all target messages(as numbers) eg.: [[2,3,4,7],[1,2,3,4]]
    # NOTE: Last of each outputs, keep EOS index, maybe 25 if there are 24 message types
    enc_inputs = []
    enc_targets = []
    dec_inputs = []
    dec_targets = []
    count = 0
    full = 0
    
    global pickle_data
    for input,out in pickle_data:
        # out[-1]=6
        output = list(map(lambda x: [[1 if x[0]==i else 0 for i in range(num_messages)],[1 if x[1]==i else 0 for i in range(num_players)]], out))
        dec_inp = list(map(lambda x: x[0]+x[1], output))
        e_inp = []
        tem = [list(map(lambda x: float(x) if(type(x)==unicode and ((not (x=='-')) and (not(x[0]==" ")))) else (0 if type(x) == unicode else x), inp)) for inp in input if len(inp)==13]
        if len(tem)>0:
            enc_inputs.append(tem)
            for i in tem:
                full += 1
                if [0]*13 == i:
                    count += 1
            # if not len(enc_inputs[-1]) == 15:
                # print enc_inputs[-1], len(enc_inputs[-1])
            enc_targets.append([None]*len(input))
            enc_targets[-1][-1] = output[0]
            dec_inputs.append(dec_inp[:-1])
            dec_targets.append(output[1:])
    
    print len(enc_inputs), count, full
    return enc_inputs, enc_targets, dec_inputs, dec_targets
    
if __name__ == "__main__":
    messages ={}
    wordvec_dim = 30
    wordvec_pickle = "./pickles/wordvec.pickle"
    nx1 = 13 # wordvec_dim
    ny1 = ny2 = 8 # len(messages)
    nx2 = 31
    ny21 = ny22 = 23
    nh1 = nh2 = 50
    
    # out = pickle.load(open("pickles/synthetic_training_data.pkl"))
    # print "Loaded outputs"
    pickle_data = pickle.load(open("pickles/training_data_full_v3.pkl")) #pickle.load(open("pickles/1milf_data.pickle"))
    shuffle(pickle_data)
    #pickle_data=pickle_data
    print "Loaded training pickle"
    out = zip(*pickle_data)[1]
    '''print "Generating....."
    start = time.time()
    final_pickle = zip(zip(*pickle_data)[0], out)
    print "Dumping...... Time taken =", str(time.time()-start)
    pickle.dump(final_pickle, open("pickles/1milf_data.pickle","w"))
    print "Done.........."
    exit()
    
    print "Generating....."
    start = time.time()
    temp = []
    for i in zip(enc_inputs[:5],out[:5]):
        temp.extend(getPermutations(i[0], i[1]))
    print "Dumping...... Time taken =", str(time.time()-start)
    pickle.dump(temp, open("pickles/permuted_data_5.pickle","w"))
    print "Done.........."
    exit()'''
    start = time.time()
    enc_inputs, enc_targets, dec_inputs, dec_targets = create_dataset(None, num_messages = 8, num_players = 23)
   
    rnn = train(nx1, nh1, ny1, ny21, nx2, nh2, ny2, ny22, enc_inputs, enc_targets, dec_inputs, dec_targets, rnn_pickle = "./pickles/rnn_cricket_changed.pickle")
    print "Done training...... Time taken =", str(time.time()-start)
    enc_targets.extend(dec_targets)
    inputs = out
    res = test(enc_inputs, targets = out, rnn_pickle = "./pickles/rnn_cricket_changed.pickle")
    print len(res)
    open("output/compare.txt","w").write("\n".join(map(str,zip(res,out))))
