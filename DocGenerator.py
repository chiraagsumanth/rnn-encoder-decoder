from flask import Flask,json, make_response, request
import cStringIO
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter
from reportlab.lib.units import inch
from reportlab.lib.colors import red, green, black, blue
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Table, TableStyle
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfbase import pdfmetrics
from reportlab.lib.enums import TA_JUSTIFY
from reportlab.lib.colors import Color
import re, os, time
from flask.ext.cors import CORS
from data import get_data_formatted
from cricketLearnerMulti import test
from msg_out import emit

app = Flask(__name__)
CORS(app)

@app.route('/api/getReport', methods=['POST'])
def get_balance():
	try:
		matchLink = request.form['match']
		with open("./cricinfoScraper/spiders/dataFetchSpider.py","r") as f1:
			code = f1.read()
		newcode = re.sub(r'start_urls = \[(.*)\]','start_urls = [ \"'+matchLink+'\" ]', code)
		with open("test.py","w") as f2:
			f2.write(newcode)
		os.remove("./cricinfoScraper/spiders/dataFetchSpider.py")
		if(os.path.isfile("crawled.json")):
			os.remove("crawled.json")
		os.system("mv test.py cricinfoScraper/spiders/dataFetchSpider.py")
		os.system("scrapy crawl dataFetchSpider -o crawled.json -t json")
		#time.sleep(7)
		#jsonDict = eval(open("crawled.json").read())[0]
	
		trng_and_testing, testing_only = get_data_formatted("crawled.json")
		results = test(testing_only, rnn_pickle = "./pickles/full_trained.pickle")
		rep = emit(results)
		#rep = emit(zip(*trng_and_testing)[1])
		print zip(*trng_and_testing)[1]
		print results
	
		output = cStringIO.StringIO()

		doc = SimpleDocTemplate(output ,pagesize=letter,
		                    rightMargin=72,leftMargin=72,
		                    topMargin=72,bottomMargin=18)
	
		# Registering custom font 
		latomedital = r"./fonts/Lato-MediumItalic.ttf"
		pdfmetrics.registerFont(TTFont("lato-med-ital", latomedital))
		latosemiital = r"./fonts/Lato-SemiboldItalic.ttf"
		pdfmetrics.registerFont(TTFont("lato-semibold-ital", latosemiital))
	
		#Empty table for a horizontal line
		styleT = TableStyle([("LINEBELOW", (0,0), (-1,-1), 1, blue),])
		t = Table([[' '*178]])
		t.setStyle(styleT)
	
		Story=[]
		styles = getSampleStyleSheet()
		styleN = styles["Normal"]
		styleH = styles["Heading1"]
		styleN.leading = 24
		styleN.fontSize = 14
		styleN.fontName = "lato-med-ital"
		styleH.fontName = "lato-semibold-ital"
		styleH.textColor = green
		styleH.fontSize = 24
		#styleH.textColor = Color(0,0,128,0.9)
		styles.add(ParagraphStyle(name='Justify', alignment=TA_JUSTIFY))
		Story.append(Paragraph("&nbsp;"*30 +"Match Report",styleH))
		Story.append(t)
		Story.append(Spacer(1,0.5*inch))
		for i in range(len(rep)):
			Story.append(Paragraph("&nbsp;"*8 + rep[i],styleN))
			Story.append(Spacer(1,0.35*inch))
		doc.build(Story)
		pdf_out = output.getvalue()
		output.close()
		response = make_response(pdf_out)
		response.headers['Content-Disposition'] = "attachment; filename='MatchReport.pdf"
		response.mimetype = 'application/pdf'
	except:
		response = make_response("""
		
		<!DOCTYPE html>
		<html lang="en">

		<head>

			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<meta name="description" content="">
			<meta name="author" content="">

			<title>Cricket Report Generator</title>
			<link rel='shortcut icon' href='http://localhost/fyp/app/landing/img/favicon.ico' type='image/x-icon'/ >
			<!-- Bootstrap Core CSS -->
			<link href="http://localhost/fyp/app/landing/css/bootstrap.min.css" rel="stylesheet">
			<link href="http://localhost/fyp/app/landing/social/bootstrap-social.css" rel="stylesheet">

			<!-- Custom CSS -->
			<link href="http://localhost/fyp/app/landing/css/landing-page.css" rel="stylesheet">

			<!-- Custom Fonts -->
		   <link href="http://localhost/fyp/app/landing/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
			<link href="http://localhost/fyp/app/landing/css/google-fonts.css" rel="stylesheet" type="text/css">
			<!--<link href="../app/chat_widget.css" rel="stylesheet" media="screen">-->  
		

			<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
			<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
			<!--[if lt IE 9]>
				<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
				<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
			<![endif]-->

		</head>
		
		<body>
			 <div class="content-section-a">
				<div class="container">

				    <div class="row">
				        <div class="col-lg-12">
				            <div class="intro-message">
				                <h2>Sorry! Match HTML differs from traning HTML format. Therefore, this match is currently unavailable.</h2>
				            </div>
				        </div>
				    </div>

				</div>
				<!-- /.container -->

			</div>
		</body>
		""")
		response.mimetype = 'text/html'
	return response
	

if __name__ == '__main__':
    app.run(host='0.0.0.0',debug=True)
