import json
from pprint import pprint
import pickle
import random

def mostrun():
	
	most_run = (msg.split("####")[0].strip()).split("\n")
	print most_run
	r = random.randint(0,len(most_run)-1)
	return most_run[r]

def mostwic():
	most_wick = (msg.split("####")[1].strip()).split("\n")
	print most_wick
	r = random.randint(0,len(most_wick)-1)
	return most_wick[r]

def mosteco():
	most_eco = (msg.split("####")[2].strip()).split("\n")
	print most_eco
	r = random.randint(0,len(most_eco)-1)
	return most_eco[r]

def came():
	cam_msg = (msg.split("####")[3].strip()).split("\n")
	print cam_msg
	r = random.randint(0,len(cam_msg)-1)
	return cam_msg[r]

def power():
	pow_msg = (msg.split("####")[4].strip()).split("\n")
	print pow_msg
	r = random.randint(0,len(pow_msg)-1)
	return pow_msg[r]

def maide():
	maid_msg = (msg.split("####")[5].strip()).split("\n")
	print maid_msg
	r = random.randint(0,len(maid_msg)-1)
	return maid_msg[r]

def part():
	part_msg = (msg.split("####")[6].strip()).split("\n")
	print part_msg
	r = random.randint(0,len(part_msg)-1)
	return part_msg[r]

def emit(oup):
# if __name__ == "__main__":
	with open('commentary.txt') as msg_file:
		global msg
		msg = msg_file.read()

	with open('crawled.json') as data_file:    
		data = json.load(data_file)

	f_in = oup[0]
	s_in = oup[1]
	
	home = data[0]['homeTeam']
	away = data[0]['awayTeam']
	venue = data[0]['venue'][0]
	date = data[0]['date']
	toss = data[0]['tossWon']
	
	t_cont_bat = (data[0]['matchNotes_firstInnings'][0]).split(" ")[1]
	if(t_cont_bat == "Lanka" or t_cont_bat == "Africa" or t_cont_bat == "Zealand" or t_cont_bat=="Indies"):
		country_bat = (data[0]['matchNotes_firstInnings'][0]).split(" ")[0] +" "+t_cont_bat
	else:
		country_bat = (data[0]['matchNotes_firstInnings'][0]).split(" ")[0]

	t_cont_bwl = (data[0]['matchNotes_secondInnings'][0]).split(" ")[1]
	if(t_cont_bwl == "Lanka" or t_cont_bwl == "Africa" or t_cont_bwl == "Zealand" or t_cont_bwl=="Indies"):
		country_bwl = (data[0]['matchNotes_secondInnings'][0]).split(" ")[0] +" "+t_cont_bwl
	else:
		country_bwl = (data[0]['matchNotes_secondInnings'][0]).split(" ")[0]
	
	if toss.strip()==country_bat.strip():
		toss = toss + ", who chose to bat first. "
	else:
		toss = toss + ", who chose to bowl first. "

	intro="The match between "+home+" and "+away+", played at "+venue+" on "+date+", saw the toss won by " + toss+"\n"
	intro = intro + " This is the "+ data[0]['competition']+". "

	partn = data[0]['highest_partnerships']
	part_1=[]
	part_2=[]
	
	for k in partn:
		if int(k[0])==1:
			if len(part_1)==0:
				part_1 = [k[2],k[5],k[6]]
				break
	
	for k in partn:
		if int(k[0])==2:
			if len(part_2)==0:
				part_2 = [k[2],k[5],k[6]]
				break


	#First Innings
	
			
	bat_f = data[0]['scorecard_first_batting']
	bwl_f = data[0]['scorecard_first_bowling']
	mn_1 = data[0]['matchNotes_firstInnings']
	tot = data[0]['total_firstInnings']
	tot_fin = data[0]['total_firstInnings']
	h_run_str=""
	h_wic_str=""
	h_eco_str=""
	pp_str=""
	cam_str=""
	maid_str=""
	part_str=""
	
	for i in f_in:
		try:
			if i[0]==0:
				h_run_ind = i[1]
				h_run = bat_f[h_run_ind]
				h_run_str = mostrun()
				h_run_str = h_run_str.replace("<name>",h_run[0])
				h_run_str = h_run_str.replace("<country>",country_bat)
				h_run_str = h_run_str.replace("<runs>",h_run[2])
				h_run_str = h_run_str.replace("<balls>",h_run[3])
				h_run_str = h_run_str.replace("<total>",tot)
			elif i[0]==1:
				h_wic_ind = i[1]-11
				h_wic = bwl_f[h_wic_ind]
				print h_wic
				h_wic_str = mostwic()
				h_wic_str = h_wic_str.replace("<name>",h_wic[0])
				h_wic_str = h_wic_str.replace("<country>",country_bwl)
				h_wic_str = h_wic_str.replace("<runs>",h_wic[3])
				h_wic_str = h_wic_str.replace("<wickets>",h_wic[4])
			elif i[0]==2:
				h_eco_ind = i[1]-11
				h_eco = bwl_f[h_eco_ind]
				h_eco_str = mosteco()
				h_eco_str = h_eco_str.replace("<name>",h_eco[0])
				h_eco_str = h_eco_str.replace("<country>",country_bwl)
				h_eco_str = h_eco_str.replace("<runs>",h_eco[3])
				h_eco_str = h_eco_str.replace("<overs>",h_eco[1])
			elif i[0]==3:
				cam_ind = i[1]
				cam = bat_f[cam_ind]
				cam_str = came()
				cam_str = cam_str.replace("<name>",cam[0])
				cam_str = cam_str.replace("<country>",country_bat)
				cam_str = cam_str.replace("<runs>",cam[2])
				cam_str = cam_str.replace("<balls>",cam[3])
				cam_str = cam_str.replace("<total>",tot)
			elif i[0]==4:
				part_str = part()
				part_str = part_str.replace("<name>",part_1[1].split("(")[0])
				part_str = part_str.replace("<name2>",part_1[2].split("(")[0])
				part_str = part_str.replace("<country>",country_bat)
				part_str = part_str.replace("<runs>",part_1[0])
			elif i[0]==5:
				pp = mn_1[1].split("-")[2].strip().split(" ")[0]
				pp_str = power()
				pp_str = pp_str.replace("<country>",country_bat)
				pp_str = pp_str.replace("<runs>",pp)
			elif i[0]==6:
				maid_ind = i[1]-11
				maid = bwl_f[maid_ind]
				maid_str = maide()
				maid_str = maid_str.replace("<name>",maid[0])
				maid_str = maid_str.replace("<country>",country_bwl)
				maid_str = maid_str.replace("<maidens>",maid[2])
		except Exception as e:
			pass
			
	#print intro,h_run_str,h_wic_str,h_eco_str,cam_str,maid_str,pp_str,part_str
	first = intro+" "+h_run_str+" "+h_wic_str+" "+h_eco_str+" "+cam_str+" "+maid_str+" "+pp_str+" "+part_str
	first_l = [intro+" "+h_run_str+" "+h_wic_str+" "+h_eco_str+" "+cam_str+" "+maid_str+" "+pp_str+" "+part_str]


	#Second Innings
	t_cont_bat = (data[0]['matchNotes_secondInnings'][0]).split(" ")[1]
	if(t_cont_bat == "Lanka" or t_cont_bat == "Africa" or t_cont_bat == "Zealand" or t_cont_bat=="Indies"):
		country_bat = (data[0]['matchNotes_secondInnings'][0]).split(" ")[0] + " " + t_cont_bat
	else:
		country_bat = (data[0]['matchNotes_secondInnings'][0]).split(" ")[0]

	t_cont_bwl = (data[0]['matchNotes_firstInnings'][0]).split(" ")[1]
	if(t_cont_bwl == "Lanka" or t_cont_bwl == "Africa" or t_cont_bwl == "Zealand" or t_cont_bwl=="Indies"):
		country_bwl = (data[0]['matchNotes_firstInnings'][0]).split(" ")[0] +" "+t_cont_bwl
	else:
		country_bwl = (data[0]['matchNotes_firstInnings'][0]).split(" ")[0]

	bat_f = data[0]['scorecard_second_batting']
	bwl_f = data[0]['scorecard_second_bowling']
	mn_2 = data[0]['matchNotes_secondInnings']
	tot = data[0]['total_secondInnings']
	h_run_str=""
	h_wic_str=""
	h_eco_str=""
	pp_str=""
	cam_str=""
	maid_str=""
	part_str=""
	
	for i in s_in:
		try:
			if i[0]==0:
				h_run_ind = i[1]
				h_run = bat_f[h_run_ind]
				h_run_str = mostrun()
				h_run_str = h_run_str.replace("<name>",h_run[0])
				h_run_str = h_run_str.replace("<country>",country_bat)
				h_run_str = h_run_str.replace("<runs>",h_run[2])
				h_run_str = h_run_str.replace("<balls>",h_run[3])
				h_run_str = h_run_str.replace("<total>",tot)
			elif i[0]==1:
				h_wic_ind = i[1]-11
				h_wic = bwl_f[h_wic_ind]
				h_wic_str = mostwic()
				h_wic_str = h_wic_str.replace("<name>",h_wic[0])
				h_wic_str = h_wic_str.replace("<country>",country_bwl)
				h_wic_str = h_wic_str.replace("<runs>",h_wic[3])
				h_wic_str = h_wic_str.replace("<wickets>",h_wic[4])
			elif i[0]==2:
				h_eco_ind = i[1]-11
				h_eco = bwl_f[h_eco_ind]
				h_eco_str = mosteco()
				h_eco_str = h_eco_str.replace("<name>",h_eco[0])
				h_eco_str = h_eco_str.replace("<country>",country_bwl)
				h_eco_str = h_eco_str.replace("<runs>",h_eco[3])
				h_eco_str = h_eco_str.replace("<overs>",h_eco[1])
			elif i[0]==3:
				cam_ind = i[1]
				cam = bat_f[cam_ind]
				cam_str = came()
				cam_str = cam_str.replace("<name>",cam[0])
				cam_str = cam_str.replace("<country>",country_bat)
				cam_str = cam_str.replace("<runs>",cam[2])
				cam_str = cam_str.replace("<balls>",cam[3])
				cam_str = cam_str.replace("<total>",tot)
			elif i[0]==4:
				part_str = part()
				part_str = part_str.replace("<name>",part_2[1].split("(")[0])
				part_str = part_str.replace("<name2>",part_2[2].split("(")[0])
				part_str = part_str.replace("<country>",country_bat)
				part_str = part_str.replace("<runs>",part_2[0])
			elif i[0]==5:
				pp = mn_2[1].split("-")[2].strip().split(" ")[0]
				pp_str = power()
				pp_str = pp_str.replace("<country>",country_bat)
				pp_str = pp_str.replace("<runs>",pp)
			elif i[0]==6:
				maid_ind = i[1]-11
				maid = bwl_f[maid_ind]
				maid_str = maide()
				maid_str = maid_str.replace("<name>",maid[0])
				maid_str = maid_str.replace("<country>",country_bwl)
				maid_str = maid_str.replace("<maidens>",maid[2])
		except Exception as e:
			pass
			
	#print intro,h_run_str,h_wic_str,h_eco_str,cam_str,maid_str,pp_str,part_str
	second = "In the second innings, "+country_bat+" came out to the middle in an attempt to chase down the target of "+tot_fin+" set by "+country_bwl+" . "+h_run_str+" "+h_wic_str+" "+h_eco_str+" "+cam_str+" "+maid_str+" "+pp_str+" "+part_str
	second_l = ["In the second innings, "+country_bat+" came out to the middle in an attempt to chase down the target set by "+country_bwl+" ."+h_run_str,h_wic_str,h_eco_str,cam_str,maid_str,pp_str,part_str]
	
	mom = (data[0]['playerOfMatch']).split("(")[0]
	res_cont = data[0]['result']

	summ = "At the end, " + res_cont
	summ = summ + ". The man of the match was awarded to "+mom+" for his scintillating display of cricket."
	
	print first + "\n\n"  + second + "\n\n" + summ

	return [first, second, summ]

