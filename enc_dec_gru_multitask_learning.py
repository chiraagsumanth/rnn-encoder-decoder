from gru_multitask_learn import GRU as MyRnn
import numpy as np

class EncoderDecoderRnn(object):
    """Implements an encoder decoder RNN pattern"""
    def __init__(self, params1, params2, output_layer=None):
        #params1 and params2 refer to the parameters for encoder and decoder respectively
        self.output_layer = output_layer
        self.encoder = MyRnn(params1[0], params1[1], params1[2], params1[3], output_layer=output_layer)
        self.decoder = MyRnn(params2[0], params2[1], params2[2], params2[3], output_layer=output_layer)
        #self.decoder = DecoderRnn(params2[0], params2[1], params2[2], output_layer=output_layer)
        self.h_enc = None
        return
    def train(self, inputs1, targets1, inputs2, targets2, max_iter=2, epochs=1, output_layer=None):
        """
            train the enc-dec with the following:
            for each seq (x1, y1, x2, y2)
                - enc forward
                - dec forward
                - dec backward
                - enc backward
        """
        iteration = 0 # p keeps track of sequence index
        while True:
            seq_num = 0
            loss = 0.0
            loss1 = 0.0
            for X1, Y1, X2, Y2 in zip(inputs1, targets1, inputs2, targets2):
                #inputs1 is a list of char sequences (word) where each element in a seq is char in one hot form
                #targets1 is a list of targets for each sequence where each element in the seq is the tgt char in 1-h form
                #inputs2 are the list of input seq to the decoder
                #targets2 are corresponding targets for the decoder
                #X1, Y1, X2, Y2 are one sequence in the list for encoder and decoder
                # eg of X1, Y1, X2, Y2 is:  Alcatel$  l  letaclA   etaclA$  (all in 1 hot form)

                theta = {'Wyh': self.encoder.Wyh, 'Wy2h': self.encoder.Wy2h, 'Wz':self.encoder.Wz,'Uz':self.encoder.Uz,'W':self.encoder.W,
                         'U':self.encoder.U,'Wr':self.encoder.Wr,'Ur':self.encoder.Ur,
                         'bz': self.encoder.bz, 'br': self.encoder.br, 'b': self.encoder.b, 'by': self.encoder.by, 'by2': self.encoder.by2}
                     

                theta1 = {'Wyh': self.decoder.Wyh, 'Wy2h': self.decoder.Wy2h, 'Wz':self.decoder.Wz,'Uz':self.decoder.Uz,'W':self.decoder.W,
                         'U':self.decoder.U,'Wr':self.decoder.Wr,'Ur':self.decoder.Ur,
                         'bz': self.decoder.bz, 'br': self.decoder.br, 'b': self.decoder.b, 'by': self.decoder.by, 'by2': self.decoder.by2}
                     
                acts = self.encoder.forward(theta, X1, Y1)
                self.encoder.final_hidden.append(acts[-1]["h"]) # this is the encoder final stage ht

                # now we would have got the ht of the final stage of the encoder - eh
                # let us now do forward of decoder
                acts1 = self.decoder.forward(theta1, X2, Y2, ht_1=acts[-1]["h"])
                
                # now we need to do the backward for decoder
                result = self.decoder.backward(theta1, Y2) 
                loss1 += result["loss"]
            
                # perform parameter update with Adagrad for DECODER
                for param, dparam, mem in zip([self.decoder.Wyh, self.decoder.Wy2h, self.decoder.Wz, self.decoder.Wr, self.decoder.W, 
                                               self.decoder.Uz, self.decoder.Ur, self.decoder.U, 
                                               self.decoder.bz, self.decoder.br, self.decoder.b, self.decoder.by, self.decoder.by2], 
                                            [result["deltas"]["Wyh"], result["deltas"]["Wy2h"], result["deltas"]["Wz"],
                                             result["deltas"]["Wr"], result["deltas"]["W"],
                                             result["deltas"]["Uz"], result["deltas"]["Ur"],
                                             result["deltas"]["U"], result["deltas"]["bz"],
                                             result["deltas"]["br"], result["deltas"]["b"], result["deltas"]["by"], result["deltas"]["by2"]], 
                                            [self.decoder.mWyh, self.decoder.mWy2h, self.decoder.mWz, self.decoder.mWr, self.decoder.mW, 
                                             self.decoder.mUz, self.decoder.mUr, self.decoder.mU, 
                                             self.decoder.mbz, self.decoder.mbr, self.decoder.mb, self.decoder.mby, self.decoder.mby2]):
                    mem += dparam * dparam
                    param += -self.decoder.alpha * dparam / np.sqrt(mem + 1e-8) # adagrad update

                # Let us do the backward for encoder, propagating from the decoder
                result = self.encoder.backward(theta, Y1, dhnext=result["dhnext"])
                loss += result["loss"]
                
                # perform parameter update with Adagrad for encoder
                for param, dparam, mem in zip([self.encoder.Wyh, self.encoder.Wy2h, self.encoder.Wz, self.encoder.Wr, self.encoder.W, 
                                               self.encoder.Uz, self.encoder.Ur, self.encoder.U, 
                                               self.encoder.bz, self.encoder.br, self.encoder.b, self.encoder.by, self.encoder.by2], 
                                            [result["deltas"]["Wyh"], result["deltas"]["Wy2h"], result["deltas"]["Wz"],
                                             result["deltas"]["Wr"], result["deltas"]["W"],
                                             result["deltas"]["Uz"], result["deltas"]["Ur"],
                                             result["deltas"]["U"], result["deltas"]["bz"],
                                             result["deltas"]["br"], result["deltas"]["b"], result["deltas"]["by"], result["deltas"]["by2"]], 
                                            [self.encoder.mWyh, self.encoder.mWy2h, self.encoder.mWz, self.encoder.mWr, self.encoder.mW, 
                                             self.encoder.mUz, self.encoder.mUr, self.encoder.mU, 
                                             self.encoder.mbz, self.encoder.mbr, self.encoder.mb, self.encoder.mby, self.encoder.mby2]):
                    mem += dparam * dparam
                    param += -self.encoder.alpha * dparam / np.sqrt(mem + 1e-8) # adagrad update
                
            if iteration % 1 == 0:
                print 'iter %d' % (iteration,) # print progress
                print "enc loss = ", loss, " dec loss = ", loss1
            iteration += 1 # iteration counter
            if iteration >= max_iter:
                print "Training completed %d iterations" % (iteration)
                break
        return #self.final_hidden
       
    def test_predict(self, inputs1, output_layer=None, max_len = 48, terminator_index = 7):
        """Predict the output sequence, use $ as the termination, 
        max seq len is given by max_len that ensures termination after finite steps"""
        # Make sure that the first result is the one which represents the message type and the second the batting index, or change the while loop appropriately(change the predicted_char value)
        final_output = []
        fout = []
        e_results = self.encoder.predict(inputs1, output_layer=output_layer, only_final = True)
        h_enc = self.encoder.get_rnn_hidden() # get the thought vectors - note predict() will overwrite
        print "len of e res = ", len(e_results) 
        for i, result in enumerate(e_results):
            if result != None:
                h_enc1 = [h_enc[i].tolist()]
                predicted_char = 1 #"a" #some char not equal to $
                seq_len = 0
                f_output = [result[-1]] # include the encoders final stage output in the result set
                index = np.argmax(result[-1][0])
                index2 = np.argmax(result[-1][1])
                fo = [tuple([index,index2])]
                vec = [0] * (len(result[-1][0]) + len(result[-1][1]))
                vec[index] = 1
                vec[len(result[-1][0])+index2] = 1 # "2-hot" representation
                inputs2 = [[vec]] #[[sm]] #d_results                    
                while predicted_char != terminator_index: #"$": # we use $ to terminate
                    if seq_len >= max_len:
                        # print "max seq len exceeded"
                        break
                    seq_len += 1
                    d_results = self.decoder.predict(inputs2, h_enc=h_enc1, output_layer=output_layer, only_final = False)
                    softmax_result = d_results[0][0][0]
                    softmax_result2 = d_results[0][0][1]
                    sm = [r[0] for r in softmax_result]
                    sm2 = [r[0] for r in softmax_result2]
                    f_output.append([softmax_result, softmax_result2])
                    index = np.argmax(softmax_result)
                    index2 = np.argmax(softmax_result2)
                    fo.append(tuple([index, index2]))
                    predicted_char = index #chr(index)
                    vec = [0] * (len(sm)+len(sm2))
                    vec[index] = 1
                    vec[len(sm)+index2] = 1
                    inputs2 = [[vec]] #[[sm]] #d_results
                    h_enc1 = [self.decoder.get_rnn_hidden()[0]]
                    #print sm, len(sm), len(h_enc1) #softmax_result
                final_output.append(f_output)
                fout.append(fo)
            else: #result none
                print "Result = None", result
        print len(final_output)
        return (e_results, final_output, fout)
