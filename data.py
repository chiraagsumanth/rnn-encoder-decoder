import json
from pprint import pprint
import pickle

def cameo(rec):
	try:
		if ((float(rec[7])>=float(200) and float(rec[4])>=6) or float(rec[7])>=float(300)):
			return 1
	except:
		return 0

def maidens(rec):
	try:
		if (float(rec[2])>0):
			return 1
	except:
		return 0

def partnership(rec):
	try:
		if(float(rec[1])>=40):
			return 1
	except:
		return 0

def get_data_formatted(fname):
	with open(fname) as data_file:    
		data = json.load(data_file)
	all_fl=[]
	for i in range(len(data)):

		#------------------------------------Scorecard-----------------------------------------
		#First Innings
		try:
			batbwl_fl1=[]
			bat_h = []
			bwl_hw = []
			bwl_me = []
			cam_1 = []
			op_1=[]
			bat_f = data[i]['scorecard_first_batting'][:-1]
			bwl_f = data[i]['scorecard_first_bowling']
			for j in bat_f:
				tmp_bat_fl = []
				tmp_bat_fl.append(bat_f.index(j))
				tmp_bat_fl.extend(j[2:8])
				tmp_bat_fl.extend([0,0,0,0,0,0])
				bat_h.append(float(tmp_bat_fl[1]))
				if(cameo(j)):
					cam_1.append([j,tmp_bat_fl[0]])
				batbwl_fl1.append(tmp_bat_fl)
			print "********************"
			op_1.append((0,bat_h.index(max(bat_h))))
		
			tmp_cam=[]
			tmp_cam_index=[]
			for c in cam_1:
				tmp_cam.append(float(c[0][7]))
				tmp_cam_index.append(c[1])

			if len(tmp_cam)>0:
				op_1.append((3,tmp_cam_index[tmp_cam.index(max(tmp_cam))]))


			bwl_ind = 11
			maid = []
			for j in bwl_f:
				tmp_bwl_fl = []
				tmp_bwl_fl.extend([0,0,0,0,0,0,0])
				tmp_bwl_fl.append(bwl_ind)
				tmp_bwl_fl.extend(j[1:6])
				bwl_hw.append(float(tmp_bwl_fl[11]))
				bwl_me.append(float(tmp_bwl_fl[12]))
				bwl_ind = bwl_ind + 1
				if(maidens(j)):
					maid.append([j,tmp_bwl_fl[7]])
				batbwl_fl1.append(tmp_bwl_fl)

			op_1.append((1,11+bwl_hw.index(max(bwl_hw))))
			op_1.append((2,11+bwl_me.index(min(bwl_me))))

			tmp_maid = []
			tmp_maid_index = []
			for m in maid:
				tmp_maid.append(float(maid[0][2]))
				tmp_maid_index.append(maid[1])

			if len(tmp_maid)>0:
				op_1.append((6,tmp_maid_index[tmp_maid.index(max(tmp_maid))]))

		except Exception as e:
			print "error",e

		#Second Innings
		try:
			batbwl_fl2=[]
			op_2=[]
			bat_h = []
			bwl_hw = []
			bwl_me = []
			cam_2 = []
			bat_f = data[i]['scorecard_second_batting'][:-1]
			bwl_f = data[i]['scorecard_second_bowling']
			for j in bat_f:
				tmp_bat_fl = []
				tmp_bat_fl.append(bat_f.index(j))
				tmp_bat_fl.extend(j[2:8])
				tmp_bat_fl.extend([0,0,0,0,0,0])
				bat_h.append(float(tmp_bat_fl[1]))
				print bat_h
				if(cameo(j)):
					cam_2.append([j,tmp_bat_fl[0]])
				batbwl_fl2.append(tmp_bat_fl)

			op_2.append((0,bat_h.index(max(bat_h))))

			tmp_cam=[]
			tmp_cam_index=[]
			for c in cam_2:
				tmp_cam.append(float(c[0][7]))
				tmp_cam_index.append(c[1])

			if len(tmp_cam)>0:
				op_2.append((3,tmp_cam_index[tmp_cam.index(max(tmp_cam))]))

			bwl_ind = 11
			maid = []
			for j in bwl_f:
				tmp_bwl_fl = []
				tmp_bwl_fl.extend([0,0,0,0,0,0,0])
				tmp_bwl_fl.append(bwl_ind)
				tmp_bwl_fl.extend(j[1:6])
				bwl_hw.append(float(tmp_bwl_fl[11]))
				bwl_me.append(float(tmp_bwl_fl[12]))
				bwl_ind = bwl_ind + 1
				if(maidens(j)):
					maid.append([j,tmp_bwl_fl[7]])
				batbwl_fl2.append(tmp_bwl_fl)

			op_2.append((1,11+bwl_hw.index(max(bwl_hw))))
			op_2.append((2,11+bwl_me.index(min(bwl_me))))

			tmp_maid = []
			tmp_maid_index = []
			for m in maid:
				tmp_maid.append(float(maid[0][2]))
				tmp_maid_index.append(maid[1])

			if len(tmp_maid)>0:
				op_2.append((6,tmp_maid_index[tmp_maid.index(max(tmp_maid))]))


		except Exception as e:
			print "error",e

		#-----------------------------------------Match Notes-----------------------------------
		#First Innings
		mn_1 = data[i]['matchNotes_firstInnings']
		try:
			pp = mn_1[1].split("-")[2].strip().split(" ")[0]
			pp_tmp = 0 
			if (float(pp)>=40):
					pp_tmp = pp_tmp+1

			if pp_tmp>0:
					op_1.append((5,22))
		except:
			pass

		#Second Innings
		mn_2 = data[i]['matchNotes_secondInnings']
		try:
			pp = mn_2[1].split("-")[2].strip().split(" ")[0]
			pp_tmp = 0 
			if (float(pp)>=40):
				pp_tmp = pp_tmp+1

			if pp_tmp>0:
				op_2.append((5,22))
		except:
			pass

		#---------------------------------------Partenrship Table------------------------------------
		ps_1 = data[i]['partnerships_firstInnings']
		try:
			par_tmp=0
			for j in ps_1:
				if (partnership(j)):
					par_tmp=par_tmp+1
		
			if par_tmp>0:
				op_1.append((4,22))
		except:
			pass

		#Second Innings
		ps_2 = data[i]['matchNotes_secondInnings']
		try:
			par_tmp=0
			for j in ps_2:
				if (partnership(j)):
					par_tmp=par_tmp+1

			if par_tmp>0:
				op_2.append((4,22))

		except:
			pass

		op_1.append((7,22))
		op_2.append((7,22))

		all_fl.append([batbwl_fl1,op_1])
		all_fl.append([batbwl_fl2,op_2])
		testing_only = [batbwl_fl1, batbwl_fl2]
	return all_fl, testing_only
#print len(all_fl)
#print all_fl[0]

#pickle.dump(all_fl,open("training_data_full_v3.pkl","w"))











